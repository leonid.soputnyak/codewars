"""
Divisors of 42 are : 1, 2, 3, 6, 7, 14, 21, 42. These divisors squared are: 1, 4, 9, 36, 49, 196, 441, 1764. 
The sum of the squared divisors is 2500 which is 50 * 50, a square!

Given two integers m, n (1 <= m <= n) we want to find all integers between m and n whose sum of squared divisors 
is itself a square. 42 is such a number.

The result will be an array of arrays or of tuples (in C an array of Pair) or a string, each subarray having two elements, 
first the number whose squared divisors is a square and then the sum of the squared divisors.

#Examples:

list_squared(1, 250) --> [[1, 1], [42, 2500], [246, 84100]]
list_squared(42, 250) --> [[42, 2500], [246, 84100]]
The form of the examples may change according to the language, see Example Tests: for more details.

Note

In Fortran - as in any other language - the returned string is not permitted to contain any redundant trailing 
whitespace: you can use dynamically allocated character strings
"""

import math


def factor_squared_amount(n):
    amount = 0
    nrt = int(math.sqrt(n))
    for i in range(1, nrt + 1):
        if not n % i:
            amount += i * i
            if int(n / i) != i:
                amount += int(n / i) ** 2
    return amount


def list_squared(m, n):
    lst = list()
    for i in range(m, n + 1):
        fsa = factor_squared_amount(i)
        frt = int(math.sqrt(fsa) * 1000)
        if fsa * 1000000 == frt * frt:  # Python supports infinite integers. I take this condition, according to task
            lst.append([i, fsa])
    return lst


# Sample Tests:
if __name__ == '__main__':
    print(list_squared(1, 250) == [[1, 1], [42, 2500], [246, 84100]])
    print(list_squared(42, 250) == [[42, 2500], [246, 84100]])
    print(list_squared(250, 500) == [[287, 84100]])
